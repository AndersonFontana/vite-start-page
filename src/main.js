import { createApp } from 'vue'
import App from '@/App.vue'
import '@/assets/style.css'

const app = createApp(App)

import common from '@/helper/common.js'
app.config.globalProperties.$common = common

app.mount('#app')
