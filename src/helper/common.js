function getImageB64(tags) {
  const unsplash_base = 'https://source.unsplash.com/768x432/?'

  return new Promise((resolve, reject) => {
    fetch(unsplash_base + tags).then((resp) => {
      if (resp.status >= 400) return resp.json().then((json) => reject({ 'status': resp.status, 'response': json }))

      resp.blob().then((blob) => {
        const reader = new window.FileReader()
        reader.readAsDataURL(blob)
        reader.onloadend = () => resolve(reader.result)
      })
    })
  })
}

export default { getImageB64 }
