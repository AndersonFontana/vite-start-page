import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

const path = require('path')
const path_src = path.resolve(__dirname, 'src')

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: { '@/': `${path_src}/` },
  },
  server: {
    host: '0.0.0.0',
    port: '8080',
  },
})
